One of the first things that any visualization tool user does when
opening a new dataset and looking at the mesh is to color the mesh with some
scalar data. Color mapping is a common visualization technique that maps data to
colo, and displays the colors in the rendered image. Of course, to map the
data array to colors, we use a transfer function. A transfer function can also
be used to map the data array to opacity for rendering translucent surfaces or
for volume rendering. This chapter desribes the basics of mapping data arrays to
color and opacity.

\section{The basics}

Color mapping (which often also includes opacity mapping) goes by various names
including scalar mapping and pseudo-coloring. The basic principle entails mapping
data arrays to colors when rendering surface meshes or volumes. Since
data arrays can have arbitrary values and types, you may want to create a mapping
to define to which color a particular data value maps. This mapping is defined
using what are called \emph{color maps} or \emph{transfer functions}. Since such
mapping from data values to rendering primitives can be defined for not just
colors, but opacity values as well, we will use the more generic term
\emph{transfer functions}.

Of course, there are cases when your data arrays indeed specify the
red-green-blue color values to use when rendering (i.e., not using a transfer
function at all). This can controlled using the \ui{Map Scalars} display property.
Refer to Chapter~\ref{sec:RenderView:DisplayProperties} for details. This
chapter relates to cases when \ui{Map Scalars} is enabled, i.e., when the transfer
function is being used to map arrays to colors and/or opacity.

In \ParaView, you can set up a transfer function for each data array
for both color and opacity separately. \ParaView associates a transfer function
with the data array identified by its name. The same transfer function is used
when coloring with the same array in different 3D views or results from
different stages in the pipeline.

For arrays with more than one component, such as vectors or tensors, you can
specify whether to use the magnitude or a specific component for the
color/opacity mapping. Similar to the transfer functions themselves, this
selection of how to map a multi-component array to colors is also associated
with the array name. Thus, two pipeline modules being colored with the arrays
that have the same name will not only be using the same transfer functions for
opacity and color, but also the component/magnitude selection.

\begin{commonerrors}
  Beginners find it easy to forget that the transfer function is associated with
  an array name and, hence, are surprised when changing the transfer function for
  a dataset being shown in one view affects other views as well. Using different
  transfer functions for the same variable is discouraged by design in \ParaView,
  since it can lead to the misinterpretation of values. If you want to use different
  transfer functions, despite this caveat, you can use a filter like the
  \ui{Calculator} or \ui{Programmable Filter} to rename the array and then use a
  different transfer function for it.
\end{commonerrors}

There are separate transfer functions for color and opacity. The opacity
transfer function is optional when doing surface renderings (You can turn it
on/off by using the \ui{Color Map Editor} as explained later\fixme{reference}),
but it gets used for volume rendering.

\subsection{Color mapping in \texttt{paraview}}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ScalarColoringOnPropertiesPanel.png}
\hspace{2em}
\includegraphics[width=0.4\linewidth]{Images/ScalarColoringFromToolbar.png}
\caption{The controls used for selecting the array to color within the
\ui{Properties} panel (left) and the \ui{Active Variables Controls} toolbar
(right).}
\label{fig:ScalarColoringControls}
\end{center}
\end{figure}

You can pick an array to use for color mapping, using either the \ui{Properties}
panel or the \ui{Active Variables Controls} toolbar. You first select the array
with which to color and then select the component or magnitude for multi-component
arrays. \ParaView will either use an existing transfer function or create a new
one for the selected array.

\subsection{Color mapping in \texttt{pvpython}}

Here's a sample script for coloring using a data array from the
\directory{disk\_out\_ref.ex2} dataset.

\begin{python}
from paraview.simple import *

# create a new 'ExodusIIReader'
reader = ExodusIIReader(FileName=['disk_out_ref.ex2'])
reader.PointVariables = ['V']
reader.ElementBlocks = ['Unnamed block ID: 1 Type: HEX8']

# show data in view
display = Show(reader)

# set scalar coloring
ColorBy(display, ('POINTS', 'V'))

# rescale color and/or opacity maps used to include current data range
display.RescaleTransferFunctionToDataRange(True)
\end{python}

The \py{ColorBy} function provided by the \py{simple} module ensures that the color
and opacity transfer functions are setup correctly for the selected array, which
is using an existing one already associated with the array name or is creating a
new one. Passing \texttt{None} as the second argument to \py{ColorBy} will
display scalar coloring.

\section{Editing the transfer functions in \texttt{paraview}}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ColorMapEditor.png}
\label{fig:colormapeditorpanel}
\caption{\ui{Color Map Editor} panel in \texttt{paraview} showing the major
components of the panel.}
\end{center}
\end{figure}

In \texttt{paraview}, you use the \ui{Color Map Editor} to customize the
color and opacity transfer functions. You can toggle the \ui{Color Map Editor}
visibility using the \menu{View > Color Map Editor} menu option.

As shown in Figure~\ref{fig:colormapeditorpanel}, the panel follows a layout
similar to the \ui{Properties} panel. The panel shows the properties for the
transfer function, if any, used for coloring the active data source (or filter)
in the active view. If the active source if not visible in the active view, or
is not employing scalar coloring, then the panel will be empty.

Similar to the Properties panel, by default, the commonly used properties are
shown. You can toggle the visibility of advanced properties by using the
\icon{Images/pqAdvanced26.png} button. Additionally, you can search for a
particular property by typing its name in the \ui{Search} box.

Whenever the transfer function is changed, we need to re-render, which may be
time consuming. By default, the panel requests renders on every change. To avoid
this, you can toggle the \icon{Images/AutoApplyIcon.png} button. When unchecked,
you will need to manually update the panel using the \ui{Render Views} button.

The \icon{Images/SP_BrowserReload.png} button restores the application
default settings for the current color map.

The \icon{Images/pqSaveArray16.png} and \icon{Images/pqSave32.png}
buttons save the current color and opacity transfer function, with all
its properties, as the default transfer function. \ParaView will use
it next time it needs to setup a transfer function to color a new data
array. The \icon{Images/pqSaveArray16.png} button saves the transfer
function as default for an array of the same name while the
\icon{Images/pqSave32.png} button saves the transfer function as
default for all arrays. Note that this will not affect transfer
functions already setup. Also this is saved across sessions,
so \ParaView will remember this even after restart.

\subsection{Mapping data}
\label{sec:ColorMapping:MappingData}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.7\linewidth]{Images/ParaView_UsersGuide_ColorMapTransferFunction.png}
\label{fig:colormaptransferfunction}
\caption{Transfer function editor and related properties}
\end{center}
\end{figure}

The \ui{Mapping Data} group of properties controls how the data is mapped to
colors or opacity. The transfer function editor widgets are used to control the
transfer function for color and opacity. The panel always shows both the
transfer functions. Whether the opacity transfer function gets used depends on
several things:
\begin{compactitem}
\item When doing surface mesh rendering, it will be used only if
  \ui{Enable opacity mapping for surfaces} is checked
\item When doing volume rendering, the opacity mapping will always be used.
\end{compactitem}

To map the data to color using a log scale, rather than a linear scale, check
the \ui{Use log scale when mapping data to colors}. It is assumed that the data
is in the non-zero, positive range. \ParaView will report errors and try to
automatically fix the range if it's ever invalid for log mapping.

Based on the user preference set in the \ui{Settings} dialog, \ParaView can
automatically rescale the transfer function every time when the user hits the
\ui{Apply} button on the \ui{Properties} panel or when the time step changes.
This behavior can be disabled by unchecking the \ui{Automatically rescale
transfer functions to fit data} checkbox. When unchecked, whatever scalar range
you specify for the transfer function will remain unchanged.

\subsection{Transfer function editor}

Using the transfer function editors is pretty straight forward. Control points
in the opacity editor widget and the color editor widget are independent of each
other. To select a control point, click on it. When selected, the control point
is highlighted with a red circle and data value associated with the control
point is shown in the Data input box under the widget. Clicking in an empty area
will add a control point at that location. To move a control point, click on the
control point and drag it. You can fine tune the data value associated with the
selected control point using the \ui{Data} input box. To delete a control point,
select the control point and then hit the \keys{\del} key. Note that the mouse pointer
should be within the transfer function widget for Qt to send the event to the
editor widget correctly. While the end control points cannot be moved or deleted, you can rescale the entire transfer function to move the control
points, as is explained later.

In the opacity transfer function widget, you can move the control points
vertically to control the opacity value associated with that control point. In
the color transfer function widget, you can double click on the control point to
pop up a color chooser widget to set the color associated with that control
point.

The opacity transfer function widget also offers some control over the
interpolation between the control points. Double click on a control point to
show the interpolation control widget, which allows for changing the sharpness and
midpoint that affect the interpolation. Click and drag the control points to
see the change in interpolation.

The several control buttons on the right side of the transfer function widgets
support the following actions:
\begin{itemize}
  \item \icon{Images/pqResetRange24.png}: Rescales the color and opacity transfer
  functions using the data range from the data source selected in the Pipeline
  browser, i.e., the active source. This rescales the entire transfer function.
  Thus, all control points including the intermediate ones are proportionally
  adjusted to fit the new range.
\item \icon{Images/pqResetRangeCustom24.png}: Rescales the color and opacity transfer
  functions using a range provided by the user. A dialog will be popped up for
  the user to enter the custom range.
\item \icon{Images/pqResetRangeTemporal24.png}: Rescales the color and
  opacity transfer functions to the range of values for data over all
  timesteps. This operation may be costly as data for all timesteps
  needs to be read.
\item \icon{Images/pqResetToVisibleRange32.png}: Rescales the color
  and opacity transfer functions using the range of values for the
  elements (cells or points) visible in the view. This operations
  assigns the entire range of colors to visible elements which may
  reveal patterns not visible otherwise.
\item \icon{Images/pqInvert24.png}: Inverts the color transfer function
  by moving the control points, e.g,. a red-to-green transfer function will be
  inverted to a green-to-red one. This only affects the color transfer function and
  leaves the opacity transfer function untouched.
\item \icon{Images/pqFavorites32.png}: Loads the color transfer function
  from a preset. Color present manager dialog pops up to allow the user to load
  presets from file, if needed. Currently, this only supports the color transfer
  function. In the future, however, it will be extended to support the opacity
  transfer function as well.
\item \icon{Images/pqSave32.png}: Saves the current color transfer
  function to presets. Color present manager dialog pops up to allow the user to
  export the transfer function to a file, if needed. Currently, this only
  supports the color transfer function. In the future, however, it will be extended
  to support the opacity transfer function as well.
\item \icon{Images/pqAdvanced26.png}: This toggles the detailed view for the
  transfer function control points. This is useful to manually enter values for
  the control points rather than using the UI.
\end{itemize}

\subsection{Color mapping parameters}
\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.8\linewidth]{Images/ParaView_UsersGuide_ColorMapParameters.png}
\label{fig:colormapparameters}
\caption{Color Mapping Parameters (showing advanced properties as well)}
\end{center}
\end{figure}

The \ui{Color Mapping Parameters} group of properties provides additional
control over the color transfer function, including control over the color
interpolation space: RGB, HSV, Lab, or diverging. When color mapping floating
point arrays with NaNs, you can select the color to use for NaN values. You can
also affect whether the color transfer function uses smooth interpolation or
a discretization of the same into a fixed number of bins.

\section{Editing the transfer functions in \texttt{pvpython}}

In \pvpython, you control the transfer functions by getting access
to the transfer function objects and then changing properties on those. The
following script shows how you can access transfer functions objects.

\begin{python}
from paraview.simple import *

# You can access the color and opacity transfer functions
# for a particular array as follows. These functions will
# create new transfer functions if none exist.
# The argument is the array name used to locate the transfer
# functions.
>>> colorMap = GetColorTransferFunction('Temp')
>>> opacityMap = GetOpacityTransferFunction('Temp')
\end{python}

Once you have access to the color and opacity transfer functions, you can change
properties on these similar to other sources, views, etc. Using the Python
tracing capabilities to discover this API is highly recommended.

\begin{python}
# Rescale transfer functions to a specific range
>>> colorMap.RescaleTransferFunction(1.0, 19.9495)
>>> opacityMap.RescaleTransferFunction(1.0, 19.9495)

# Invert the color map.
>>> colorMap.InvertTransferFunction()

# Map color map to log-scale preserving relative positions for
# control points
>>> colorMap.MapControlPointsToLogSpace()
>>> colorMap.UseLogScale = 1

# Return back to linear space.
>>> colorMap.MapControlPointsToLinearSpace()
>>> colorMap.UseLogScale = 0

# Change using of opacity mapping for surfaces
>>> colorMap.EnableOpacityMapping = 1

# Explicitly specify color map control points
# The value is a flattened list of tuples
# (data-value, red, green, blue). The color components
# must be in the range [0.0, 1.0]
>>> colorMap.RGBPoints = [1.0, 0.705, 0.015, 0.149,
                          5.0, 0.865, 0.865, 0.865,
                         10.0, 0.627, 0.749, 1.0,
                         19.9495, 0.231373, 0.298039, 0.752941]

# Similarly, for opacity map. The value here is
# a flattened list of (data-value, opacity, mid-point, sharpness)
>>> opacity.Points = [1.0, 0.0, 0.5, 0.0,
                      9.0, 0.404, 0.5, 0.0,
                     19.9495, 1.0, 0.5, 0.0]

# Note, in both these cases the controls points are assumed to be sorted
# based on  the data values. Also, not setting the first and last
# control point to have same data value can have unexpected artifacts
# in the 'Color Map Editor' panel.
\end{python}

Oftentimes, you want to rescale the color and opacity maps to fit the
current data ranges. You can do this as follows:

\begin{python}
>>> source = GetActiveSource()

# Update the pipeline, if hasn't been updated already.
>>> source.UpdatePipeline()

# First, locate the display properties for the source of interest.
>>> display = GetDisplayProperties()

# Reset the color and opacity maps currently used by 'display' to
# using the range for the array 'display' is set to color with.
# This requires that the 'display' has been set to use scalar coloring
# using an array that is available in the data generated. If not, you will
# get errors.
>>> display.RescaleTransferFunctionToDataRange()
\end{python}

\section{Color legend}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.5\linewidth]{Images/ParaView_UsersGuide_ColorMapLegendDetail.png}
\label{fig:colormaplegenddetail}
\caption{Color legend in \ParaView.}
\end{center}
\end{figure}

The color legend, also known as scalar bar or color bar, is designed to provide
the user information about which color corresponds to what data value in the
rendered view. You can toggle the visibility of the color legend corresponding
to the transfer function being shown/edit in the \ui{Color Map Editor} by using the
\icon{Images/ParaView_UsersGuide_ColorLegendButton.png} button near the top of
the panel. This button affects the visibility of the legend in the active view.

Figure~\ref{fig:colormaplegenddetail} shows the various components of the color
legend. By default, the title is typically the name of the array (and component
number or magnitude for non-scalar data arrays) being mapped. On one side are
the automatically generated labels, while on the other side are the annotations,
including the start and end annotations, which correspond to the data range
being mapped.

You can control the characteristics of the color legend using the
\icon{Images/pqEditScalarBar16.png} button. The color legend is interactive. You
can click and drag the legend to place it interactively at any position in the
view. Additionally, you can change the length of the legend by dragging the
end-markers shown when you hover the mouse pointer over the legend.

\subsection{Color legend parameters}
\label{sec:ColorLegendParameters}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UsersGuide_ColorMapLegendParameters.png}
\label{fig:colormaplegendparameters}
\caption{\ui{Edit Color Legend Parameters} dialog in \paraview.}
\end{center}
\end{figure}

You can edit the color legend parameters by clicking on the
\icon{Images/pqEditScalarBar16.png} button on the \ui{Color Map Editor} panel. This
will pop up a dialog that shows the available parameters. Any changes made only
affect the particular color legend in the active view.

Besides the obvious changing of title text and font properties for the title,
labels, and annotations, some of the other parameters that the user can change
are as follows: \ui{Draw Annotations} controls whether the annotations (including the
start and end annotations) are to be drawn at all.

When checked, \ui{Draw NaN Annotations} results in the color legend showing the
NaN color setup in the \ui{Color Map Editor} panel in a separate box right beside the
color legend. The annotation text shown for that box can be modified by changing
the \ui{NaN Annotation} text.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.3\linewidth]{Images/ParaView_UsersGuide_ColorMapNaN.png}
\label{fig:colormapNaN}
\caption{Color legend showing NaN annotation}
\end{center}
\end{figure}

If \ui{Automatic Label Format} is checked, \ParaView will try to pick an optimal
representation for numerical values based on the value and available screen
space. By unchecking it, you can explicitly specify the \texttt{printf}-style
format to use for numeric values. Number of Labels is a suggestion for maximum
number of labels to show on the legend. Note that this is a suggestion and may not
exactly match the number of labels shown. To explicitly label values of
interest, use the annotations discussed later in this post. \ui{Aspect Ratio} is used
to control the thickness of the legend: The greater the number, the thinner the legend.

\subsection{Color legend in \texttt{pvpython}}

To show the color legend or scalar bar for the transfer function used for scalar
mapping a source in a view, you can use API on its display properties:

\begin{python}
>>> source = ...
>>> display = GetDisplayProperties(source, view)

# to show the color legend
>>> display.SetScalarBarVisibility(view, True)

# to hide the same
>>> display.SetScalarBarVisibility(view, False)
\end{python}

To change the color legend properties as in
Section~\ref{sec:ColorLegendParameters}, you need to first get access to the
color legend object for the color transfer function in a particular view. These
are analogous to display properties for a source in a view.

\begin{python}
>>> colorMap = GetColorTransferFunction('Temp')

# get the scalar bar in a view (akin to GetDisplayProperties)
>>> scalarBar = GetScalarBar(colorMap, view)

# Now, you can change properties on the scalar bar object.
>>> scalarBar.TitleFontSize = 8
>>> scalarBar.DrawNanAnnotation = 1
\end{python}

\section{Annotations}

Simply put, annotations allow users to put custom text at particular data values
in the color legend. The min and max data mapped value annotations are
automatically added. To add any other custom annotations, you can use the
\ui{Color Map Editor}.

Since annotations is an advanced parameter, you have to either toggle the
visibility of advanced properties using the \icon{Images/pqAdvanced26.png} icon
near the top of the panel or simply search for \texttt{annotations} in the
search box. That will show the \ui{Annotations} widget, which is basically a
list widget where users can enter key-value pairs, rather than value-annotation
pairs, as shown in Figure~\ref{fig:colormapwidget}.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/ParaView_UsersGuide_ColorMapWidget.png}
\label{fig:colormapwidget}
\caption{Widget to add/edit annotations on the \ui{Color Map Editor} panel}
\end{center}
\end{figure}

You can use the buttons of the right on the widget to add/remove entries. Enter
the data value to annotate under the \ui{Value} column and then enter the text to use
under the \ui{Annotation} column.

You can use the \keys{\tab} key to edit the next entry. Hitting \keys{\tab} after
editing the last entry in the table will automatically result in adding a new
row, thus, making it easier to add bunch of annotations without having to click
any buttons.

There are two reasons in which annotation texts may not show up on the
legend: First, the value added is outside the mapped range of the color transfer
function; second, \ui{Draw Annotations} is unchecked in the \ui{Color Legend Parameters
dialog}.

The \icon{Images/pqFilter32.png} and
\icon{Images/pqFilterEyeball16.png} buttons can be used to fill the
annotations widget with unique discrete values from a data array, if
possible. Based on the number of distinct values present in the data
array, this may not yield any result (Instead, a warning message will
be shown). The data array comes either from the selected source object
if you use the \icon{Images/pqFilter32.png} button or it comes from
one of visible pipeline objects if you use the
\icon{Images/pqFilterEyeball16.png} button.

\subsection{Annotations in \texttt{pvpython}}

Annotations is a property on the color map object. You simply get access to the
color map of interest and then change the \py{Annotations} property.

\begin{python}
>>> colorMap = GetColorTransferFunction('Temp')

# Annotations are specified as a flattened list of tuples
# (data-value, annotation-text)
>>> colorMap.Annotations = ['1', 'Slow',
                            '10', 'Fast']
\end{python}

\section{Categorical colors}

A picture is worth a thousand words, they say, so let's just let the picture do
the talking. Categorical color maps allow you to render visualizations as shown
in Figure~\ref{fig:colormapcategorical}.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/ParaView_UsersGuide_ColorMapCategorical.jpg}
\label{fig:colormapcategorical}
\caption{Visualization using a categorical color map for discrete coloring}
\end{center}
\end{figure}

When one thinks of scalar coloring, one is typically talking of mapping
numerical values to colors. However, in some cases, the numerical values are not
really numbers, but enumerations such as elements types and gear types (as in
Figure~\ref{fig:colormapcategorical}) or generally, speaking, categories. The
traditional approach of using an interpolated color map specifying the range of
values with which to color doesn't really work here. While users could always play
tricks with the number of discrete steps, multiple control points, and annotations,
it is tedious and cumbersome.

Categorical color maps provide an elegant solution for
such cases. Instead of a continuous color transfer function, the user specifies a
set of discrete values and colors to use for those values. For any element where
the data value matches the values in the lookup table exactly, \ParaView renders
the specified color; otherwise, the NaN color is used.

The color legend or scalar bar also switches to a new mode where it renders
swatches with annotations, rather than a single bar. You can add custom
annotations for each value in the lookup table.

\subsubsection{Categorical Color: User Interface}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.7\linewidth]{Images/ParaView_UsersGuide_ColorMapCatEditor.png}
\label{fig:colormapcategoricaleditor}
\caption{Default Color Map Editor when Interpret Values As Categories is checked}
\end{center}
\end{figure}

To tell \ParaView that the data array is to be treated as categories for coloring,
check the \ui{Interpret Values As Categories} checkbox in the \ui{Color Map
Editor} panel. As soon as that's checked, the panel switches to categorical mode:
The \ui{Mapping Data} group is hidden, and the \ui{Annotations} group becomes a
non-advanced group, i.e., the annotations widget is visible even if the panel is
not showing advanced properties, as is shown in
Figure~\ref{fig:colormapcategoricaleditor}.

The annotations widget will still show any annotations that may have been added
earlier, or it may be empty if none were added. You can add annotations for data values
as was the case before using the buttons on the side of the widget. This time, however, each
annotation entry also has a column for color. If color has not been specified, a
question mark icon will show up; otherwise, a color swatch will be shown. You can
double click the color swatch or the question mark icon to specify the color to
use for that entry. Alternatively, you can choose from a preset collection of
categorical color maps by clicking the \icon{Images/pqFavorites32.png} button.

%% \begin{figure}[htb]
%\begin{center}
%\includegraphics[width=0.4\linewidth]{Images/ParaView_UsersGuide_ColorMapPreset.png}
%\label{fig:colormappreset}
%\caption{Choose preset dialog for categorical color maps}
%\end{center}
%\end{figure}

As before, you can use \keys{\tab} key to edit and add multiple values. Hence,
you can first add all the values of interest in one pass and then pick a preset
color map to set colors for the values added. If the preset has fewer colors than
the annotated values, then the user may have to manually set the colors for those
extra annotations.

Also \icon{Images/pqFilter32.png} plays the same role as described in the previous
post: Fill the annotations widget with unique discrete values from the data
array, is possible. Based on the number of distinct values present in the data
array, this may not yield any result (instead, a warning message will be shown.)

\begin{commonerrors}
Categorical color maps are designed for data arrays with enumerations, which are
typically integer arrays. However, they can be used for arrays with floating
point numbers as well. With floating point numbers, the value specified
for annotation may not match the value in the dataset exactly, even when the user
expects it to match. In that case, the NaN color will be used. In the future, we
may add a mechanism to specify a delta to use to when comparing data values to
annotated values.
\end{commonerrors}

\subsubsection{Categorical colors in \texttt{pvpython}}

\begin{python}
>>> categoricalColorMap = GetColorTransferFunction('Modes')
>>> categoricalColorMap.InterpretValuesAsCategories = 1

# specify the labels for the categories. This is similar to how
# other annotations are specified.
>>> categoricalColorMap.Annotations = ['0', 'Alpha', '1', 'Beta']

# now, set the colors for each category. These are an ordered list
# of flattened tuples (red, green, blue). The first color gets used for
# the first annotation, second for second, and so on
>>> categoricalColorMap.IndexedColors = [0.0, 0.0, 0.0,
                                         0.89, 0.10, 0.10]
\end{python}
