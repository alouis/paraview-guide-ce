\section{VTK data model}
\label{sec:VTKDataModel}

To use \ParaView effectively, you need to understand the \ParaView data model.
\ParaView uses VTK, the Visualization Toolkit, to provide the visualization
and data processing model. This chapter briefly introduces the VTK
data model used by \ParaView. For more details, refer to one of the VTK books.

The most fundamental data structure in VTK is a data object. Data objects can
either be scientific datasets, such as rectilinear grids or finite elements meshes
(see below), or more abstract data structures, such as graphs or trees. These
datasets are formed from smaller building blocks: mesh (topology and geometry) and
attributes.

\subsection{Mesh}
\label{sec:VTKDataModel:Mesh}

Even though the actual data structure used to store the mesh in memory depends
on the type of the dataset, some abstractions are common to all types. In
general, a mesh consists of vertices (points\keyword{Points}) and
cells\keyword{Cells} (elements, zones). Cells
are used to discretize a region and can have various types such as tetrahedra,
hexahedra, etc. Each cell contains a set of vertices. The mapping from cells to
vertices is called the connectivity. Note that even though it is possible to
define data elements such as faces and edges, VTK does not represent these
explicitly. Rather, they are implied by a cell's type and by its connectivity. One
exception to this rule is the arbitrary polyhedron, which explicitly stores its
faces. Figure~\ref{fig:ExampleMesh} is an example mesh that consists of two cells.
The first cell is defined by vertices $(0, 1, 3, 4)$, and the second cell is
defined by vertices $(1, 2, 4, 5)$. These cells are neighbors because they share
the edge defined by the points $(1, 4)$.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Cells.png}
\caption{Example of a mesh.}
\label{fig:ParaView_UG_Cells}
\end{center}
\end{figure}


A mesh is fully defined by its topology and the spatial coordinates of its
vertices. In VTK, the point coordinates may be implicit, or they may be explicitly defined by
a data array of dimensions $(number\_of\_points \times 3)$.

\subsection{Attributes (fields, arrays)}
\label{sec:VTKDataModel:Attributes}

An attribute \keyword{Attribute}\keyword{Data Array}\fixme{Attribute \& data array are in the UI
index, but are defined here. Remove one or both keyword tags?} (or a data array or field)
defines the discrete values of a field
over the mesh. Examples of attributes include pressure, temperature, velocity,
and stress tensor. Note that VTK does not specifically define different types of
attributes. All attributes are stored as data arrays, which can have an arbitrary
number of components. \ParaView makes some assumptions in regards to the number
of components. For example, a 3-component array is assumed to be an array of
vectors. Attributes can be associated with points or cells. It is also possible
to have attributes that are not associated with either.
Figure~\ref{fig:PointCenteredAttributes} demonstrates the use of a point-centered
attribute. Note that the attribute is only defined on the vertices.
Interpolation is used to obtain the values everywhere else. The interpolation
functions used depend on the cell type. See the VTK documentation for details.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Cells_with_values.png}
\caption{Point-centered attribute in a data array or field.}
\label{fig:PointCenteredAttributes}
\end{center}
\end{figure}

Figure~\ref{fig:CellCenteredAttributes} demonstrates the use of a cell-centered
attribute. Note that cell-centered attributes are assumed to be constant over
each cell. Due to this property, many filters in VTK cannot be directly applied
to cell-centered attributes. It is normally required to apply a Cell Data to
Point Data filter. In \ParaView, this filter is applied automatically, when
necessary.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Cells_with_cvalues.png}
\caption{Cell-centered attribute.}
\label{fig:CellCenteredAttributes}
\end{center}
\end{figure}

\subsection{Uniform rectilinear grid (image data)}
\label{sec:VTKDataModel:UniformRectilinearGrid}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Image.png}
\caption{Sample uniform rectilinear grid.}
\label{fig:UniformRectilinearGrid}
\end{center}
\end{figure}

A uniform rectilinear grid, or image data, defines its topology and point
coordinates implicitly (Figure~\ref{fig:UniformRectilinearGrid}). To fully define
the mesh for an image data, VTK uses the following:

\begin{compactenum}
\item \emph{Extents} - These define the minimum and maximum indices in each
direction. For example, an image data of extents $(0, 9)$, $(0, 19)$, $(0, 29)$
has 10 points in the x-direction, 20 points in the y-direction, and 30 points in the
z-direction. The total number of points is $10 \times 20 \times 30$.
\item \emph{Origin} - This is the position of a point defined with indices $(0, 0, 0)$.
\item \emph{Spacing} - This is the distance between each point. Spacing for each
direction can defined independently.
\end{compactenum}

The coordinate of each point is defined as follows: $coordinate = origin +
index \times spacing$ where coordinate, origin, index, and spacing are vectors of
length 3.

Note that the generic VTK interface for all datasets uses a flat index. The
$(i,j,k)$ index can be converted to this flat index as follows:
$idx\_flat = k \times (npts_x \times npts_y) + j \times nptr_x + i$.

A uniform rectilinear grid consists of cells of the same type. This type is determined by the dimensionality of the dataset (based on the extents) and can either be vertex (0D), line (1D), pixel (2D), or voxel (3D).

Due to its regular nature, image data requires less storage than other datasets.
Furthermore, many algorithms in VTK have been optimized to take advantage of
this property and are more efficient for image data.

\subsection{Rectilinear grid}
\label{sec:VTKDataModel:RectilinearGrid}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Rectilinear.png}
\caption{Rectilinear grid.}
\label{fig:ExampleMesh}
\end{center}
\end{figure}

A rectilinear grid, such as Figure~\ref{fig:ExampleMesh}, defines its topology
implicitly and point coordinates semi-implicitly. To fully define the mesh for a
rectilinear grid, VTK uses the following:

\begin{compactenum}
\item \emph{Extents} - These define the minimum and maximum indices in each
direction. For example, a rectilinear grid of extents $(0, 9)$, $(0, 19)$, $(0,
29)$ has 10 points in the x-direction, 20 points in the y-direction, and 30
points in the z-direction. The total number of points is $10 \times 20 \times 30$.
\item \emph{Three arrays defining coordinates in the x-, y- and z-directions} -
These arrays are of length $npts_x$, $npts_y$, and $npts_z$. This is a significant
savings in memory, as the total memory used by these arrays is
$npts_x+npts_y+npts_z$ rather than $npts_x \times npts_y \times npts_z$.
\end{compactenum}

The coordinate of each point is defined as follows:

$coordinate = (coordinate\_array_x(i), coordinate\_array_y(j), coordinate\_array_z(k))$.

Note that the generic VTK interface for all datasets uses a flat index. The
$(i,j,k)$ index can be converted to this flat index as follows:
$idx\_flat = k \times (npts_x \times npts_y) + j \times nptr_x + i$.

A rectilinear grid consists of cells of the same type. This type is determined
by the dimensionality of the dataset (based on the extents) and can either be
vertex (0D), line (1D), pixel (2D), or voxel (3D).

\subsection{Curvilinear grid (structured grid)}
\label{sec:VTKDataModel:CurvilinearGrid}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Curvilinear.png}
\caption{Curvilinear or structured grid.}
\label{fig:CurvilinearGrid}
\end{center}
\end{figure}

A curvilinear grid, such as Figure~\ref{fig:CurvilinearGrid}, defines its
topology implicitly and point coordinates explicitly. To fully define the mesh
for a curvilinear grid, VTK uses the following:

\begin{compactenum}
\item \emph {Extents} - These define the minimum and maximum indices in each
direction. For example, a curvilinear grid of extents $(0, 9)$, $(0, 19)$, $(0,
29)$ has $10 \times 20 \times 30$ points regularly defined over a curvilinear mesh.
\item \emph {An array of point coordinates} - This array stores the position of
each vertex explicitly.
\end{compactenum}

The coordinate of each point is defined as follows:
$coordinate = coordinate\_array(idx\_flat)$.
The $(i,j,k)$ index can be converted to this flat index as follows:
$idx\_flat = k \times (npts_x \times npts_y) + j \times npts_x + i$.

A curvilinear grid consists of cells of the same type. This type is determined
by the dimensionality of the dataset (based on the extents) and can either be
vertex (0D), line (1D), quad (2D), or hexahedron (3D).

\subsection{AMR dataset}
\label{sec:VTKDataModel:AMRDataset}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_AMR.png}
\caption{AMR dataset.}
\label{fig:AMRDataset}
\end{center}
\end{figure}

VTK natively supports Berger-Oliger type \AMR datasets,
as shown in Figure~\ref{fig:AMRDataset}. An \AMR dataset is essentially a
collection of uniform rectilinear grids grouped under increasing refinement
ratios (decreasing spacing). VTK's \AMR dataset does not force any constraint on
whether and how these grids should overlap. However, it provides support for
masking (blanking) sub-regions of the rectilinear grids using an array of bytes.
This allows VTK to process overlapping grids with minimal artifacts. VTK can
automatically generate the masking arrays for Berger-Oliger compliant meshes.

\subsection{Unstructured grid}
\label{sec:VTKDataModel:UnstructuredGrid}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Unstructured.png}
\caption{Unstructured grid.}
\label{fig:UnstructuredGrid}
\end{center}
\end{figure}

An unstructured grid, such as Figure~\ref{fig:UnstructuredGrid}, is the most
general primitive dataset type. It stores topology and point coordinates
explicitly. Even though VTK uses a memory-efficient data structure to store the
topology, an unstructured grid uses significantly more memory to represent its
mesh. Therefore, use an unstructured grid only when you cannot represent your
dataset as one of the above datasets. VTK supports a large number of cell types,
all of which can exist (heterogeneously) within one unstructured grid. The full
list of all cell types supported by VTK can be found in the file vtkCellType.h
in the VTK source code. Here is the list of cell types and their numeric values
as of when this document was written:

\begin{multicols}{2}
\begin{compactitem}
\item VTK\_EMPTY\_CELL = 0
\item VTK\_VERTEX = 1
\item VTK\_POLY\_VERTEX = 2
\item VTK\_LINE = 3
\item VTK\_POLY\_LINE = 4
\item VTK\_TRIANGLE = 5
\item VTK\_TRIANGLE\_STRIP = 6
\item VTK\_POLYGON = 7
\item VTK\_PIXEL = 8
\item VTK\_QUAD = 9
\item VTK\_TETRA = 10
\item VTK\_VOXEL = 11
\item VTK\_HEXAHEDRON = 12
\item VTK\_WEDGE = 13
\item VTK\_PYRAMID = 14
\item VTK\_PENTAGONAL\_PRISM = 15
\item VTK\_HEXAGONAL\_PRISM = 16
\item VTK\_QUADRATIC\_EDGE = 21
\item VTK\_QUADRATIC\_TRIANGLE = 22
\item VTK\_QUADRATIC\_QUAD = 23
\item VTK\_QUADRATIC\_TETRA = 24
\item VTK\_QUADRATIC\_HEXAHEDRON = 25
\item VTK\_QUADRATIC\_WEDGE = 26
\item VTK\_QUADRATIC\_PYRAMID = 27
\item VTK\_BIQUADRATIC\_QUAD = 28
\item VTK\_TRIQUADRATIC\_HEXAHEDRON = 29
\item VTK\_QUADRATIC\_LINEAR\_QUAD = 30
\item VTK\_QUADRATIC\_LINEAR\_WEDGE = 31
\item VTK\_BIQUADRATIC\_QUADRATIC\_WEDGE = 32
\item VTK\_BIQUADRATIC\_QUADRATIC\_HEXAHEDRON = 33
\item VTK\_BIQUADRATIC\_TRIANGLE = 34
\item VTK\_CUBIC\_LINE = 35
\item VTK\_CONVEX\_POINT\_SET = 41
\item VTK\_POLYHEDRON = 42
\item VTK\_PARAMETRIC\_CURVE = 51
\item VTK\_PARAMETRIC\_SURFACE = 52
\item VTK\_PARAMETRIC\_TRI\_SURFACE = 53
\item VTK\_PARAMETRIC\_QUAD\_SURFACE = 54
\item VTK\_PARAMETRIC\_TETRA\_REGION = 55
\item VTK\_PARAMETRIC\_HEX\_REGION = 56
\item VTK\_HIGHER\_ORDER\_EDGE = 60
\item VTK\_HIGHER\_ORDER\_TRIANGLE = 61
\item VTK\_HIGHER\_ORDER\_QUAD = 62
\item VTK\_HIGHER\_ORDER\_POLYGON = 63
\item VTK\_HIGHER\_ORDER\_TETRAHEDRON = 64
\item VTK\_HIGHER\_ORDER\_WEDGE = 65
\item VTK\_HIGHER\_ORDER\_PYRAMID = 66
\item VTK\_HIGHER\_ORDER\_HEXAHEDRON = 67
\end{compactitem}
\end{multicols}

Many of these cell types are straightforward. For details, see the VTK
documentation.

\subsection{Polygonal grid (polydata)}
\label{sec:VTKDataModel:PolyData}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Polydata.png}
\caption{Polygonal grid.}
\label{fig:PolyData}
\end{center}
\end{figure}

A polydata, such as Figure~\ref{fig:PolyData}, is a specialized version of an
unstructured grid designed for efficient rendering. It consists of 0D cells
(vertices and polyvertices), 1D cells (lines and polylines), and 2D cells
(polygons and triangle strips). Certain filters that generate only these cell
types will generate a polydata. Examples include the Contour and Slice filters.
An unstructured grid, as long as it has only 2D cells supported by polydata, can
be converted to a polydata using the \ui{Extract Surface Filter}. A polydata can be
converted to an unstructured grid using \ui{Clean to Grid}.

\subsection{Table}
\label{sec:VTKDataModel:Table}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.7\linewidth]{Images/ParaView_UG_Table.png}
\caption{Table.}
\label{fig:Table}
\end{center}
\end{figure}

A table, such as Figure~\ref{fig:Table}, is a tabular dataset that consists of rows
and columns. All chart views have been designed to work with tables. Therefore,
all filters that can be shown within the chart views generate tables. Also,
tables can be directly loaded using various file formats such as the comma-separated
values format. Tables can be converted to other datasets as long as
they are of the right format. Filters that convert tables include Table to
Points and Table to Structured Grid.

\subsection{Multiblock dataset}
\label{sec:MultiblockDataset}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Multiblock.png}
\caption{Multiblock dataset.}
\label{fig:MultiblockDataset}
\end{center}
\end{figure}

You can think of a multi-block dataset (Figure~\ref{fig:MultiblockDataset}) as a
tree of datasets where the leaf nodes are \emph{simple} datasets. All of the
data types described above, except \AMR, are \emph{simple} datasets. Multi-block
datasets are used to group together datasets that are related. The relation
between these datasets is not necessarily defined by \ParaView. A multi-block
dataset can represent an assembly of parts or a collection of meshes of
different types from a coupled simulation. Multi-block datasets can be loaded or
created within \ParaView using the Group filter. Note that the leaf nodes of a
multi-block dataset do not all have to have the same attributes. If you apply a
filter that requires an attribute, it will be applied only to blocks that have
that attribute.

\subsection{Multipiece dataset}
\label{sec:MultipieceDataset}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ParaView_UG_Multipiece.png}
\caption{Multipiece dataset.}
\label{fig:MultipieceDataset}
\end{center}
\end{figure}

Multi-piece datasets, such as Figure~\ref{fig:MultipieceDataset}, are similar to
multi-block datasets in that they group together simple datasets. There is one key
difference. Multi-piece datasets group together datasets that are part of a
whole mesh - datasets of the same type and with the same attributes. This data
structure is used to collect datasets produced by a parallel simulation without
having to append the meshes together. Note that there is no way to create a
multi-piece dataset within \ParaView. It can only be created by using certain readers.
Furthermore, multi-piece datasets act, for the most part, as simple datasets.
For example, it is not possible to extract individual pieces or to obtain
information about them.
